/*Padr�o de projeto muito utilizado
 * Inten��o: GARANTIR QUE UMA DETERMINADA CLASSE TENHA UMA E SOMENTE UMA
 * INST�NCIA, MANTENDO UM PONTO GLOBAL DE ACESSO */

package singleton;

import java.awt.Dimension;

import javax.swing.JFrame;

//Singleton
public class Janela extends JFrame{
	private static Janela j = null;
	
	private Janela(){
		setPreferredSize(new Dimension(640, 480));
	}
	
	public static Janela getIstance(){
		if(j==null){
			j = new Janela();
		}return j;
	}

}
